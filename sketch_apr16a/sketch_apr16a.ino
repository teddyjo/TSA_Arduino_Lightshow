#define RGB1DELAY 1430
#define RGB2DELAY 2000
#define RGB3DELAY 770

unsigned long last1switch = 0;
unsigned long last2switch = 0;
unsigned long last3switch = 0;

int idx1 = 0;
int idx2 = 0;
int idx3 = 0;
int nextidx1 = 0;
int nextidx2 = 0;
int nextidx3 = 0;

int pin4val = 0;
int pin5val = 0;
int pin6val = 0;
int pin7val = 0;
int pin8val = 0;
int pin9val = 0;
int pin10val = 0;
int pin11val = 0;
int pin12val = 0;

float ang1 = 0;
float ang2 = 0;
float ang3 = 0;

float mod(float x, float y) {
  if(x < y) {
    return x;
  }
  else {
    return mod(x - y, y);
  }
}

float max_of(float x, float y) {
  if(x > y) {
    return x;
  }
  return y;
}

int RGB_to_pixel(float r, float g, float b) {
  int rp = 255 - ((int) (255.0 * r));
  int gp = 255 - ((int) (255.0 * g));
  int bp = 255 - ((int) (255.0 * b));
  return (rp << 16) | (gp << 8) | bp;
}

int extract_red(int x) {
  return (x >> 16) & 0xFF;
}

int extract_green(int x) {
  return (x >> 8) & 0xFF;
}

int extract_blue(int x) {
  return x & 0xFF;
}

int HSV_to_RGB1(float theta) {
  if(theta == 0) {
    return RGB_to_pixel(1.0, 0.0, 0.0);
  }
  if(theta > 0 && theta < 120) {
    float delta = theta / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(lambda, delta, 0.0);
  }
  if(theta == 120) {
    return RGB_to_pixel(0.0, 1.0, 0.0);
  }
  if(theta > 120 && theta < 240) {
    float delta = (theta - 120) / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(0.0, lambda, delta);
  }
  if(theta == 240) {
    return RGB_to_pixel(0.0, 0.0, 1.0);
  }
  if(theta > 240 && theta < 360) {
    float delta = (theta - 240) / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(delta, 0.0, lambda);
  }
}

int HSV_to_RGB2(float theta) {
  if(theta == 0) {
    return RGB_to_pixel(0.0, 1.0, 0.0);
  }
  if(theta > 0 && theta < 120) {
    float delta = theta / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(0.0, lambda, delta);
  }
  if(theta == 120) {
    return RGB_to_pixel(0.0, 0.0, 1.0);
  }
  if(theta > 120 && theta < 240) {
    float delta = (theta - 120) / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(delta, 0.0, lambda);
  }
  if(theta == 240) {
    return RGB_to_pixel(1.0, 0.0, 0.0);
  }
  if(theta > 240 && theta < 360) {
    float delta = (theta - 240) / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(lambda, delta, 0.0);
  }
}

int HSV_to_RGB3(float theta) {
  if(theta == 0) {
    return RGB_to_pixel(0.0, 0.0, 1.0);
  }
  if(theta > 0 && theta < 120) {
    float delta = theta / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(delta, 0.0, lambda);
  }
  if(theta == 120) {
    return RGB_to_pixel(1.0, 0.0, 0.0);
  }
  if(theta > 120 && theta < 240) {
    float delta = (theta - 120) / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(lambda, delta, 0.0);
  }
  if(theta == 240) {
    return RGB_to_pixel(0.0, 1.0, 0.0);
  }
  if(theta > 240 && theta < 360) {
    float delta = (theta - 240) / 120;
    float lambda = 1 - delta;
    float chi = max_of(delta, lambda);
    delta *= (1 / chi);
    lambda *= (1 / chi);
    return RGB_to_pixel(0.0, lambda, delta);
  }
}

void display_pinset_color(int pin, int rpinval, int gpinval, int bpinval) {
  analogWrite(pin + 0, rpinval);
  analogWrite(pin + 1, gpinval);
  analogWrite(pin + 2, bpinval);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);

  Serial.begin(9600);
  
  // 2 & 3 are constant LOW output
  // 4-6 are RGB #1
  // 7-9 are RGB #2
  // 10-12 are RGB #3
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(2, LOW);
  digitalWrite(3, LOW);
  
  
  
  ang1 += 0.2;
  
  ang1 = mod(ang1, 360.0);
  int color = HSV_to_RGB1((float)ang1);
  pin4val = extract_red(color);
  pin5val = extract_green(color);
  pin6val = extract_blue(color);
  
  display_pinset_color(4, pin4val, pin5val, pin6val);
  
  
  
  ang2 += 0.6;
  
  ang2 = mod(ang2, 360.0);
  color = HSV_to_RGB2((float)ang2);
  pin7val = extract_red(color);
  pin8val = extract_green(color);
  pin9val = extract_blue(color);
  
  display_pinset_color(7, pin7val, pin8val, pin9val);
  
  
  
  ang3 += 1;
  
  ang3 = mod(ang3, 360.0);
  color = HSV_to_RGB3((float)ang3);
  pin10val = extract_red(color);
  pin11val = extract_green(color);
  pin12val = extract_blue(color);
  
  display_pinset_color(10, pin10val, pin11val, pin12val);
  
  delay(25);
}
